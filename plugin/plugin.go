package plugin

import (
	"math"
	"os"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/go-plugin"

	internal "gitlab.com/gitlab-org/fleeting/fleeting/internal/plugin"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
	"google.golang.org/grpc"
)

func Serve(impl provider.InstanceGroup) {
	logger := hclog.New(&hclog.LoggerOptions{
		Level:      hclog.Trace,
		Output:     os.Stderr,
		JSONFormat: true,
	})

	plugin.Serve(&plugin.ServeConfig{
		HandshakeConfig: internal.Handshake,
		Logger:          logger,
		VersionedPlugins: map[int]plugin.PluginSet{
			0: {
				"instancegroup": &internal.GRPCInstanceGroupPlugin{
					Logger: logger,
					Impl:   impl,
				},
			},
		},
		GRPCServer: func(opts []grpc.ServerOption) *grpc.Server {
			opts = append(opts, grpc.MaxRecvMsgSize(math.MaxInt32))
			opts = append(opts, grpc.MaxSendMsgSize(math.MaxInt32))
			return plugin.DefaultGRPCServer(opts)
		},
	})
}
