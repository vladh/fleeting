package main

import (
	"gitlab.com/gitlab-org/fleeting/fleeting/plugin"
	dummy "gitlab.com/gitlab-org/fleeting/fleeting/plugin/fleeting-plugin-dummy"
)

func main() {
	plugin.Serve(&dummy.InstanceGroup{})
}
