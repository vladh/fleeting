package connector

import (
	"context"
	"errors"
	"net"
	"strings"
	"time"

	"golang.org/x/crypto/ssh"

	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
)

type sshClient struct {
	client *ssh.Client
}

func DialSSH(ctx context.Context, info provider.ConnectInfo, options DialOptions) (*sshClient, error) {
	addr := info.InternalAddr
	if options.UseExternalAddr && (info.ExternalAddr != "" || addr == "") {
		addr = info.ExternalAddr
	}
	addr = hostport(addr, "22")

	config := &ssh.ClientConfig{
		User:            info.Username,
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Timeout:         info.Timeout,
	}

	if info.Key == nil {
		config.Auth = append(config.Auth, ssh.Password(info.Password))
	} else {
		signer, err := ssh.ParsePrivateKey(info.Key)
		if err != nil {
			return nil, err
		}
		config.Auth = append(config.Auth, ssh.PublicKeys(signer))
	}

	var client *ssh.Client
	var err error
	for i := 0; i < 5; i++ {
		client, err = getSSHClient(ctx, dialer{
			Timeout:   info.Timeout,
			KeepAlive: info.Keepalive,
			DialFn:    options.DialFn,
		}, addr, config)
		if err != nil && strings.Contains(err.Error(), "handshake failed") {
			time.Sleep(2 * time.Second)
			continue
		}
		break
	}
	if err != nil {
		return nil, err
	}

	return &sshClient{client: client}, nil
}

func (c *sshClient) Close() error {
	return c.client.Close()
}

func (c *sshClient) Run(ctx context.Context, opts RunOptions) error {
	sess, err := c.client.NewSession()
	if err != nil {
		return err
	}
	defer sess.Close()

	sess.Stdin = opts.Stdin
	sess.Stdout = opts.Stdout
	sess.Stderr = opts.Stderr

	errCh := make(chan error, 1)

	go func() {
		errCh <- sess.Run(opts.Command)
	}()

	select {
	case <-ctx.Done():
		return sess.Close()
	case err := <-errCh:
		return c.newExitError(err)
	}
}

func (c *sshClient) newExitError(err error) error {
	if err == nil {
		return nil
	}

	var sshExitErr *ssh.ExitError
	if errors.As(err, &sshExitErr) {
		return &ExitError{
			err:      err,
			exitCode: sshExitErr.ExitStatus(),
		}
	}

	return &ExitError{err: err, exitCode: 0}
}

func (c *sshClient) Dial(n string, addr string) (net.Conn, error) {
	return c.client.Dial(n, addr)
}

func getSSHClient(ctx context.Context, d dialer, addr string, config *ssh.ClientConfig) (*ssh.Client, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	conn, err := d.DialContext(ctx, "tcp", addr)
	if err != nil {
		return nil, err
	}

	c, chans, reqs, err := ssh.NewClientConn(conn, addr, config)
	if err != nil {
		return nil, err
	}

	return ssh.NewClient(c, chans, reqs), nil
}
