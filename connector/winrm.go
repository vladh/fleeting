package connector

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/masterzen/winrm"
	"github.com/masterzen/winrm/soap"

	"gitlab.com/gitlab-org/fleeting/fleeting/connector/internal/proxy"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
)

const tunnelScript = `try { [System.IO.File]::WriteAllBytes('fleeting-proxy.exe', [System.Convert]::FromBase64String('%s')) } catch {}`

type winRMClient struct {
	client *winrm.Client
	info   provider.ConnectInfo
}

func DialWinRM(ctx context.Context, info provider.ConnectInfo, opts DialOptions) (*winRMClient, error) {
	addr := info.InternalAddr
	if opts.UseExternalAddr && (info.ExternalAddr != "" || addr == "") {
		addr = info.ExternalAddr
	}

	host, sport, err := net.SplitHostPort(hostport(addr, "5985"))
	if err != nil {
		return nil, fmt.Errorf("parsing address: %w", err)
	}

	port, err := net.LookupPort("tcp", sport)
	if err != nil {
		return nil, fmt.Errorf("resolving port: %w", err)
	}

	endpoint := winrm.NewEndpoint(host, port, false, false, nil, nil, nil, info.Timeout)

	scheme := "http"
	if endpoint.HTTPS {
		scheme = "https"
	}

	client, err := winrm.NewClientWithParameters(
		winrm.NewEndpoint(host, port, false, false, nil, nil, nil, info.Timeout),
		info.Username,
		info.Password,
		&winrm.Parameters{
			Timeout:      "PT60S",
			EnvelopeSize: 5 * 1024 * 1024,
			TransportDecorator: func() winrm.Transporter {
				return &transport{
					ctx:      ctx,
					info:     info,
					url:      fmt.Sprintf("%s://%s:%d/wsman", scheme, endpoint.Host, endpoint.Port),
					username: info.Username,
					password: info.Password,
					dialFn:   opts.DialFn,
				}
			},
		},
	)
	if err != nil {
		return nil, fmt.Errorf("creating client: %w", err)
	}

	return &winRMClient{client: client, info: info}, nil
}

func (c *winRMClient) Close() error {
	return nil
}

func (c *winRMClient) Run(ctx context.Context, opts RunOptions) error {
	exitCode, err := c.client.RunWithContextWithInput(ctx, opts.Command, opts.Stdout, opts.Stderr, opts.Stdin)
	if exitCode == 0 && err == nil {
		return nil
	}

	return &ExitError{err: err, exitCode: exitCode}
}

func (c *winRMClient) Dial(n string, addr string) (net.Conn, error) {
	bin := proxy.ReadBinary(c.info.OS, c.info.Arch)
	if bin == nil {
		return nil, fmt.Errorf("found no suitable tunnel binary (os:%s, arch:%s)", c.info.OS, c.info.Arch)
	}

	script := fmt.Sprintf(tunnelScript, base64.StdEncoding.EncodeToString(bin))
	_, err := c.client.RunWithInput("powershell.exe -NoProfile -NonInteractive -ExecutionPolicy Bypass -Command -", os.Stdout, os.Stderr, strings.NewReader(script))
	if err != nil {
		return nil, fmt.Errorf("winrm sending tunnel program: %w", err)
	}

	connReader, w := io.Pipe()
	r, connWriter := io.Pipe()

	go func() {
		_, err := c.client.RunWithInput(fmt.Sprintf("fleeting-proxy.exe %s %s", n, addr), w, os.Stderr, r)
		if err != nil {
			err = fmt.Errorf("winrm tunnel: %w", err)
			r.CloseWithError(err)
			w.CloseWithError(err)
		}
	}()

	return &rwConn{connWriter, connReader}, nil
}

// transport provides a context cancelable winrm.Transporter
type transport struct {
	info      provider.ConnectInfo
	transport *http.Transport
	url       string
	username  string
	password  string
	ctx       context.Context
	dialFn    func(ctx context.Context, network string, addr string) (net.Conn, error)
}

func (c *transport) Transport(endpoint *winrm.Endpoint) error {
	if c.info.Keepalive == 0 {
		c.info.Keepalive = 30 * time.Second
	}

	d := dialer{
		Timeout:   endpoint.Timeout,
		KeepAlive: c.info.Keepalive,
		DialFn:    c.dialFn,
	}

	c.transport = &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: endpoint.Insecure,
			ServerName:         endpoint.TLSServerName,
		},
		DialContext:           d.DialContext,
		ResponseHeaderTimeout: endpoint.Timeout,
	}

	if len(endpoint.CACert) > 0 {
		c.transport.TLSClientConfig.RootCAs = x509.NewCertPool()
		if !c.transport.TLSClientConfig.RootCAs.AppendCertsFromPEM(endpoint.CACert) {
			return fmt.Errorf("unable to read certificates")
		}
	}

	return nil
}

func (c *transport) Post(client *winrm.Client, request *soap.SoapMessage) (string, error) {
	httpClient := &http.Client{Transport: c.transport}

	req, err := http.NewRequestWithContext(c.ctx, http.MethodPost, c.url, strings.NewReader(request.String()))
	if err != nil {
		return "", fmt.Errorf("impossible to create http request %w", err)
	}

	req.Header.Set("Content-Type", "application/soap+xml;charset=UTF-8")
	req.SetBasicAuth(c.username, c.password)

	resp, err := httpClient.Do(req)
	if err != nil {
		return "", fmt.Errorf("unknown error %w", err)
	}
	defer resp.Body.Close()
	defer io.Copy(io.Discard, io.LimitReader(resp.Body, 16*1024))

	if strings.Contains(resp.Header.Get("Content-Type"), "application/soap+xml") {
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			return "", fmt.Errorf("error while reading request body %w", err)
		}
		return string(body), nil
	}

	return "", fmt.Errorf("invalid content type")
}

type rwConn struct {
	io.WriteCloser
	io.Reader
}

func (conn *rwConn) Close() error {
	conn.WriteCloser.Close()

	return nil
}

func (*rwConn) LocalAddr() net.Addr                { return addr{} }
func (*rwConn) RemoteAddr() net.Addr               { return addr{} }
func (*rwConn) SetDeadline(t time.Time) error      { return fmt.Errorf("unsupported") }
func (*rwConn) SetReadDeadline(t time.Time) error  { return fmt.Errorf("unsupported") }
func (*rwConn) SetWriteDeadline(t time.Time) error { return fmt.Errorf("unsupported") }

type addr struct{}

func (addr) Network() string { return "gocat.Conn" }
func (addr) String() string  { return "gocat.Conn" }
