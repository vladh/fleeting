package main

import (
	"errors"
	"fmt"
	"io"
	"net"
	"time"
)

func do(w io.Writer, r io.Reader, conn net.Conn, timeout time.Duration, closedErr error) error {
	errChan := make(chan error)
	go func() {
		_, err := io.Copy(conn, r)
		if err != nil {
			err = fmt.Errorf("proxy r to conn: %w", err)
		}
		errChan <- err
	}()

	go func() {
		_, err := io.Copy(w, conn)
		if errors.Is(err, closedErr) {
			err = nil
		}
		if err != nil {
			err = fmt.Errorf("proxy w to conn: %w", err)
		}
		errChan <- err
	}()

	err1 := <-errChan
	err2 := conn.Close()
	err3 := <-errChan

	if err1 != nil {
		return err1
	}
	if err2 != nil {
		return err2
	}
	return err3
}
