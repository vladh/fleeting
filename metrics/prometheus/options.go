package prometheus

import (
	"github.com/prometheus/client_golang/prometheus"
)

var (
	defInstanceTimeBuckets         = []float64{1, 5, 10, 15, 30, 45, 60, 90, 120, 180}
	defInstanceLifeDurationBuckets = []float64{60, 300, 600, 900, 1800, 3600, 21600, 43200, 86400}
)

type Option func(*options)

type options struct {
	constLabels                  prometheus.Labels
	instanceCreationTimeBuckets  []float64
	instanceIsRunningTimeBuckets []float64
	instanceDeletionTimeBuckets  []float64
	instanceLifeDurationSeconds  []float64
}

func loadOptions(opts *options, provided []Option) {
	opts.constLabels = prometheus.Labels{}
	opts.instanceCreationTimeBuckets = defInstanceTimeBuckets
	opts.instanceIsRunningTimeBuckets = defInstanceTimeBuckets
	opts.instanceDeletionTimeBuckets = defInstanceTimeBuckets
	opts.instanceLifeDurationSeconds = defInstanceLifeDurationBuckets

	for _, o := range provided {
		o(opts)
	}
}

func WithConstLabels(constLabels prometheus.Labels) Option {
	return func(o *options) {
		o.constLabels = constLabels
	}
}

func WithInstanceCreationTimeBuckets(buckets []float64) Option {
	return func(o *options) {
		if len(buckets) == 0 {
			return
		}

		o.instanceCreationTimeBuckets = buckets
	}
}

func WithInstanceIsRunningTimeBuckets(buckets []float64) Option {
	return func(o *options) {
		if len(buckets) == 0 {
			return
		}

		o.instanceIsRunningTimeBuckets = buckets
	}
}

func WithInstanceDeletionTimeBuckets(buckets []float64) Option {
	return func(o *options) {
		if len(buckets) == 0 {
			return
		}

		o.instanceDeletionTimeBuckets = buckets
	}
}

func WithInstanceLifeDurationBuckets(buckets []float64) Option {
	return func(o *options) {
		if len(buckets) == 0 {
			return
		}

		o.instanceLifeDurationSeconds = buckets
	}
}
