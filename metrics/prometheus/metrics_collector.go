package prometheus

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

type metricsCollector struct {
	maxInstances          prometheus.Gauge
	internalOperations    *prometheus.CounterVec
	instanceOperations    *prometheus.CounterVec
	instances             *prometheus.GaugeVec
	missedUpdates         prometheus.Counter
	instanceCreationTime  prometheus.Histogram
	instanceIsRunningTime prometheus.Histogram
	instanceDeletionTime  prometheus.Histogram
	instanceLifeDuration  prometheus.Histogram
}

func New(opts ...Option) *metricsCollector {
	var o options
	loadOptions(&o, opts)

	f := newFactory("fleeting", "provisioner", o.constLabels)

	return &metricsCollector{
		maxInstances:          f.NewGauge("max_instances", "Maximum number of instances allowed for fleeting to maintain"),
		internalOperations:    f.NewCounterVec("internal_operations_total", "Counter of total internal operations handled by provisioner", []string{"operation"}),
		instanceOperations:    f.NewCounterVec("instance_operations_total", "Counter of total operations performed on instances", []string{"operation"}),
		instances:             f.NewGaugeVec("instances", "Current number of instances in different states", []string{"state"}),
		missedUpdates:         f.NewCounter("missed_updates_total", "Counter of total times an instance reached the 'missed_update' state"),
		instanceCreationTime:  f.NewHistogram("instance_creation_time_seconds", "Histogram of instance creation times", o.instanceCreationTimeBuckets),
		instanceIsRunningTime: f.NewHistogram("instance_is_running_time_seconds", "Histogram of duration for the instance to ender running state", o.instanceIsRunningTimeBuckets),
		instanceDeletionTime:  f.NewHistogram("instance_deletion_time_seconds", "Histogram of instance deletion times", o.instanceDeletionTimeBuckets),
		instanceLifeDuration:  f.NewHistogram("instance_life_duration_seconds", "Histogram of instance's life durations", o.instanceLifeDurationSeconds),
	}
}

func (m *metricsCollector) MaxInstancesSet(v int) {
	m.maxInstances.Set(float64(v))
}

func (m *metricsCollector) InternalOperationInc(operation string) {
	m.internalOperations.WithLabelValues(operation).Inc()
}

func (m *metricsCollector) InstanceOperationInc(operation string) {
	m.instanceOperations.WithLabelValues(operation).Inc()
}

func (m *metricsCollector) InstancesCountSet(state string, v int) {
	m.instances.WithLabelValues(state).Set(float64(v))
}

func (m *metricsCollector) MissedUpdateInc() {
	m.missedUpdates.Inc()
}

func (m *metricsCollector) InstanceCreationTimeObserve(d time.Duration) {
	m.instanceCreationTime.Observe(d.Seconds())
}

func (m *metricsCollector) InstanceIsRunningTimeObserve(d time.Duration) {
	m.instanceIsRunningTime.Observe(d.Seconds())
}

func (m *metricsCollector) InstanceDeletionTimeObserve(d time.Duration) {
	m.instanceDeletionTime.Observe(d.Seconds())
}

func (m *metricsCollector) InstanceLifeDurationObserve(d time.Duration) {
	m.instanceLifeDuration.Observe(d.Seconds())
}

func (m *metricsCollector) Describe(ch chan<- *prometheus.Desc) {
	m.maxInstances.Describe(ch)
	m.internalOperations.Describe(ch)
	m.instanceOperations.Describe(ch)
	m.instances.Describe(ch)
	m.missedUpdates.Describe(ch)
	m.instanceCreationTime.Describe(ch)
	m.instanceIsRunningTime.Describe(ch)
	m.instanceDeletionTime.Describe(ch)
	m.instanceLifeDuration.Describe(ch)
}

func (m *metricsCollector) Collect(ch chan<- prometheus.Metric) {
	m.maxInstances.Collect(ch)
	m.internalOperations.Collect(ch)
	m.instanceOperations.Collect(ch)
	m.instances.Collect(ch)
	m.missedUpdates.Collect(ch)
	m.instanceCreationTime.Collect(ch)
	m.instanceIsRunningTime.Collect(ch)
	m.instanceDeletionTime.Collect(ch)
	m.instanceLifeDuration.Collect(ch)
}
