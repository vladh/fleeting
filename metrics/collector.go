package metrics

import (
	"time"
)

const (
	InstanceOperationCreate = "create"
	InstanceOperationDelete = "delete"
)

const (
	InternalOperationRemoval   = "removal"
	InternalOperationReconcile = "reconcile"
	InternalOperationProvision = "provision"
)

const (
	InstanceCountRequested = "requested"
	InstanceCountPending   = "pending"
	InstanceCountCreating  = "creating"
	InstanceCountRunning   = "running"
	InstanceCountDeleting  = "deleting"
)

type Collector interface {
	MaxInstancesSet(v int)

	InternalOperationInc(operation string)
	InstanceOperationInc(operation string)
	InstancesCountSet(state string, v int)

	MissedUpdateInc()

	InstanceCreationTimeObserve(d time.Duration)
	InstanceIsRunningTimeObserve(d time.Duration)
	InstanceDeletionTimeObserve(d time.Duration)
	InstanceLifeDurationObserve(d time.Duration)
}

func Init(mc Collector, maxInstances int) Collector {
	if mc == nil {
		return nil
	}

	mc.MaxInstancesSet(maxInstances)

	mc.InstancesCountSet(InstanceCountRequested, 0)
	mc.InstancesCountSet(InstanceCountPending, 0)
	mc.InstancesCountSet(InstanceCountCreating, 0)
	mc.InstancesCountSet(InstanceCountRunning, 0)
	mc.InstancesCountSet(InstanceCountDeleting, 0)

	return mc
}
